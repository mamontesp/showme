#include <iostream>
#include <math.h>
using namespace std;
/*int primo (int numero)
{
int divisores;
int prueba;
while (prueba<=numero || divisores>2){
prueba++;
    if (numero%prueba==0){
    divisores++;
    }
}

if (divisores>2){
return 0;
}
else{
return=1;
}
}
*/

int modulo(int a, int b)
{
int aux;
do
{
aux=a-b;
a=aux;
}
while(a>=b);
return a;
}

int div(int a, int b){
int i=0;
while (a>=b){
i++;
a=a-b;
}
return i;
}

int MCD(int a, int b)
{
int iaux; //auxiliar
int i1=a>b?a:b;
//max(a,b);
int i2=a<=b?a:b;
//min(a,b);
do{
iaux=i2;
//i2=i1-i2*(i1/i2)
i2=modulo(i1,i2);
i1=iaux;
}while(i2!=0);
return i1;
}

void calculoTemp(int Tcal, int Tfr, int Ts, int &fr1, int &fr2)
{
//CambioTemperaturaPerdida
int templost;
//CambioTemperaturaGanada
int tempgain;

templost=Tcal-Ts;
tempgain=Ts-Tfr;

int mcd=MCD(templost,tempgain);
fr1=div(templost,mcd);
fr2=div(tempgain,mcd);
}

int main()
{
int Tcal=50;
int Tfr=10;
int Ts;
//output
int fr1;
int fr2;
    cout << "Ingrese Temperatura" << endl;
    cin >> Ts;
    calculoTemp (Tcal, Tfr, Ts, fr1, fr2);
    cout << "Las Fracciones de masa son" << endl;
    cout << fr1 << endl;
    cout << fr2 << endl;
    return 0;
}
