`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:52:47 11/09/2015 
// Design Name: 
// Module Name:    Crono 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Crono(
    	clk,
	Start,
	Reset,
	resetCrono,
	Parcial_1,
	valorTiempo,
	P1s1,
	P1s2,
	P1min1,
	P1min2
    );

input clk;
	input Start;
	input Reset;
	input resetCrono;
	input Parcial_1; //variable que guarda el tiempo después del baño
	output reg [31:0] valorTiempo;  //Variable de muestra en segmentos
	output reg [2:0] P1s1=4'd0; //se agrego output reg a estas 4
        output reg [3:0] P1s2=4'd0;
        output reg [3:0] P1min1=4'd0;
        output reg [3:0] P1min2=4'd0;



//Variables divisores de frecuencia
reg clkb=1;
reg C_1Hz=1;
reg [25:0] counter10K=0;
reg [25:0] counter=0;

//Variables de estado
reg [2:0] estado=0;
reg [2:0] estado_sig=0;




//Variables de tiempo


// Variables auxiliares de guardado

reg [3:0] s1=4'b0;
reg [2:0] s2=3'b0;
reg [3:0] min1=4'b0;
reg [3:0] min2=4'b0;


//condicional para guardar parciales
reg C1=0;


always @(posedge clk)
begin
	//Frecuencia de 10kHz para mostrar los segmentos al tiempo
	if(counter10K == 26'd4_999)
		begin
		counter10K=0;
		clkb=~clkb;
		end
	else
		begin
		counter10K = counter10K +1;
		end
	
	// Frecuencia de 1Hz para el reloj
	if(counter == 26'd49_999_999)
		begin
		counter=0;
		C_1Hz=~C_1Hz;
		end
	else
		begin
		counter = counter +1;
		end
end

always @(posedge C_1Hz)
	begin
		if(!Reset && !resetCrono && Start)
			begin
					if(s1 == 4'd9)
						begin //Begin de s1
							s1 <=0;
							if(s2 == 4'd5)
								begin
									s2 <=0;
									if(min1 == 4'd9)
										begin
											min1 <=0;
											if(min2 == 4'd9)
												begin
												min2 <=0;
												end
											else 
												begin
												min2 <= min2+1;
												end
										end //end min1
									 else
										begin
										min1 <= min1+1;
										end
									end //end s2
								else
									begin
									s2 <= s2 +1;
									end
						end // End de s1
						else
							begin
							s1 <= s1+1;
							end
			end //if reset
			else
				begin
					min2<=0;
					min1<=0;
					s2<=0;
					s1<=0;
				end	
	end // end del always

always @(posedge clkb)
begin
	if(Reset || resetCrono)
		begin
		C1=0;
		end
	if(Start)
		begin
			case(estado)
			0: 
				begin							 // se guarda en seg_temp la milesima 1 de cada parcial
					estado_sig=3'd1;
					if(Parcial_1==1)
						begin
						valorTiempo[7:0]=P1s1;

						end
					else 
						begin
						valorTiempo[7:0]=s1;
						end
				end
			1: 
				begin
					estado_sig=3'd2;
					if(Parcial_1==1)
						begin
						valorTiempo[15:8]=P1s2;
						end
					else 
						begin
						valorTiempo[15:8]=s2;
						end
				end
			2: 
				begin
					estado_sig=3'd3;
					if(Parcial_1==1)
						begin
						valorTiempo[23:16]= P1min1;
						end
					else 
						begin
						valorTiempo[23:16] = min1;
						end
						
				end
			3: 
				begin
					estado_sig=3'd0;
					if(Parcial_1==1)
						begin
						valorTiempo[31:24] = P1min2;
						end
					else			
						begin
						valorTiempo[31:24] = min2;
						end
				end
			endcase //Termina case de variables de estado
		estado = estado_sig;
		// Para guardar los numeros
		if(Parcial_1 && C1==0)
			begin
			P1s1<=s1;
			P1s2<=s2;
			P1min1<=min1;
			P1min2<=min2;
			C1=C1+1;
			end
		end //del if
		else
			begin
			C1=0;
			end
		
end //del always



endmodule
