//-----------------------------------------------------------------
// ONEWIRE Master
//-----------------------------------------------------------------
module wb_onewire #(
  parameter AW =  2,              // address width
  parameter DW = 32,              // data    width
  parameter SW = DW/8,             // select  width
  parameter OWN = 1		   // 1-wire lines	
)(
  input clk, //clock
  input rst, //reset
  // Wishbone master port
  input  wire          wb_cyc,    // cycle
  input  wire          wb_stb,    // strobe
  input  wire          wb_we,     // write enable
  input  wire [AW-1:0] wb_adr,    // address
  input  wire [SW-1:0] wb_sel,    // byte select
  input  wire [DW-1:0] wb_dat_w,  // write data
  output wire [DW-1:0] wb_dat_r,  // read  data
  output wire          wb_ack,    // acknowledge
  output wire          wb_err,    // error
  output wire          wb_rty,    // retry
  // slave port
  // input  wire [DW-1:0] bus_rdt,    // read  data
  // 1-wire interface
  output [OWN-1:0] owr_p,    // output power enable
  output [OWN-1:0] owr_e,    // output pull down enable
  input  [OWN-1:0] owr_i,     // input from bidirectional wire
  output prueba
);
assign prueba=owr_i[0];
wire          bus_wen;   // write enable
wire          bus_ren;   // read  enable
wire [AW-1:0] bus_adr;   // address
wire [DW-1:0] bus_wdt;   // write data unused for temp sensor
wire [DW-1:0] bus_rdt;    // read  data
// bus write and read enable

assign bus_wen = wb_cyc & wb_stb &  wb_we;
assign bus_ren = wb_cyc & wb_stb & ~wb_we;

// address
assign bus_adr = wb_adr;

// write data
assign bus_wdt = wb_dat_w;

// read data
assign wb_dat_r = bus_rdt;

// error if not full width access else acknowledge
assign wb_ack =  &wb_sel;
assign wb_err = ~&wb_sel;
assign wb_rty =     1'b0;

sockit_owm oneWire (
  // system signals
	.clk1(clk),
	.rst(rst),
  // CPU bus interface
	.bus_ren(bus_ren),  // read  enable
	.bus_wen(bus_wen),  // write enable
	.bus_adr(bus_adr),  // address
	.bus_wdt(bus_wdt),  // write data
	.bus_rdt(bus_rdt),  // read  data
	.bus_irq(bus_irq),  // interrupt request
  // 1-wire interface
	.owr_p(owr_p),    // output power enable
	.owr_e(owr_e),    // output pull down enable
	.owr_i(owr_i)     // input from bidirectional wire
);

endmodule
