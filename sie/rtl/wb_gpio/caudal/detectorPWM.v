`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:00:08 04/11/2016 
// Design Name: 
// Module Name:    risingedge_detector 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module detectorPWM(
			input sig_a, 
			input rst, 
			input clk,
			output reg sig_a_risedge,
			output  [31:0] contador,
			input activacion
);


reg [2:0] sig_a_d;
reg [7:0] n1;
reg [7:0] n2;
reg [7:0] n3;
reg [7:0] n4;

always @(posedge clk or negedge rst)

begin

    if(!rst)
      sig_a_d<=1'b0;
    else
      sig_a_d<=sig_a;
end

always @(posedge clk) 	
begin
	if( activacion==1)
		begin
 		sig_a_risedge<=sig_a && ~sig_a_d;
		end
	else sig_a_risedge<=0;
end

always @(posedge sig_a_risedge)
	begin
	if(!rst)
		begin
				if(n1 == 4'd9)
					begin //este
						n1 <=0;
						if(n2 == 4'd9)
							begin
								n2 <=0;
								if(n3 == 4'd9)
									begin
										n3 <=0;
										if(n4 == 4'd9)
											begin
											n4 <=0;
											end
										else 
											begin
											n4 <= n4+1;
											end
									end //end min1
								 else
									begin
									n3 <= n3+1;
									end
								end //end s2
							else
								begin
								n2 <= n2 +1;
								end
					end // este
					else
						begin
						n1 <= n1+1;
						end
			
		end //if reset
		else
			begin
				n4<=0;
				n3<=0;
				n2<=0;
				n1<=0;
			end	
end // end del always
	
assign contador[7:0]=n1;
assign contador[15:8]=n2;
assign contador[23:16]=n3;
assign contador[31:24]=n4;



endmodule
