`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:12:40 11/19/2015 
// Design Name: 
// Module Name:    Matrix_Keyboard 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Matrix_Keyboard(
		input clk_out,
		output reg [3:0]column, //Columnas de la matriz numerica
		input [3:0]files,			//Filas de la matriz numerica
		output reg[7:0] value 	// numero oprimido
		);

initial begin
		value <=8'h17;
		column <= 4'd1;
end

always@ (posedge clk_out)
		case(column)
				4'd1:	begin
							case(files)
									4'd1: value <=8'h01;
									4'd2: value <=8'h04;
									4'd4: value <=8'h07;
									4'd8: value <=8'h25;
							endcase
							column<=2*column;
						end
				4'd2:	begin
							case(files)
									4'd1: value <=8'h02;
									4'd2: value <=8'h05;
									4'd4: value <=8'h08;
									4'd8: value <=8'h00;
							endcase
							column<=2*column;
						end
				4'd4:	begin
							case(files)
									4'd1: value <=8'h03;
									4'd2: value <=8'h06;
									4'd4: value <=8'h09;
									4'd8: value <=8'h11;
							endcase
							column<= 2*column;
						end 
				4'd8:	begin
							case(files)
									4'd1: value <=8'h0A;
									4'd2: value <=8'h0B;
									4'd4: value <=8'h0C;
									4'd8: value <=8'h0D;
							endcase
							column<= 4'd1;
						end
				default: value <=8'h17;
				endcase

endmodule

