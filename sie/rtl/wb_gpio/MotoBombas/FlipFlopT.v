`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:11:57 04/14/2016 
// Design Name: 
// Module Name:    FlipFlopT 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FlipFlopT(reloj,reset,Q);
output Q;
input reloj,reset;
reg Q;
initial begin Q=1'b0; end

always @ (posedge reloj)
if(~reset)
begin
Q=~Q; 
end
else
Q=0;
endmodule
