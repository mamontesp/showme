`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:58:38 04/17/2016 
// Design Name: 
// Module Name:    activacionMotobombas 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module activacionMotobombas(reloj, 
reset, 
fr1, 
fr2, 
activacion, 
calentamiento,
relojM1, 
relojM2, 
relojM3);

input reloj;
input reset;
input [7:0] fr1;
input [7:0] fr2;
input activacion;
input calentamiento;

output relojM1;
output relojM2;
output relojM3;

assign relojM1=relojM1aux;//&&activacion;
assign relojM2=relojM2aux;//&&activacion;
assign relojM3=relojM3aux;//&&activacion;

//Para probar
//parameter f1=7;
//parameter f2=3;

wire [7:0] intervalo;
assign intervalo=fr1+fr2;

wire relojSegundos;
reg [5:0] contadorSegundos;
reg relojM1aux;
reg relojM2aux;
reg relojM3aux;

relojMotobombas relojseg(reloj, reset, relojSegundos);

always @(posedge relojSegundos)
if (~reset)
begin
	contadorSegundos<=contadorSegundos+1;
	if(calentamiento)
	begin
		if (contadorSegundos<fr1)
		begin
		relojM1aux<=1'b1;
		relojM2aux<=1'b0;
		end
		else 
		begin
			if (contadorSegundos<intervalo)
			begin	
			relojM1aux<=1'b0;
			relojM2aux<=1'b1;
			end
			else
			contadorSegundos<=1'b0;
		end
	end

	else
	relojM1aux<=1'b0;

	if (activacion)
	begin
	relojM3aux<=1'b1;
	end
	else
	relojM3aux<=1'b0;
end
else
	begin
	relojM1aux<=1'b0;
	relojM2aux<=1'b0;
	relojM3aux<=1'b0;
	contadorSegundos<=1'b0;
	end
endmodule
