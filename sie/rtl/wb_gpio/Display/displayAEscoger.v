`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:23:14 04/10/2016 
// Design Name: 
// Module Name:    displayAEscoger 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module displayAEscoger(reloj, reset, display);
input reloj;
input reset;
output [7:0] display;
 
wire reloj1;
wire reloj2;
wire reloj_v;

reg[7:0] display1;

assign display=display1;

relojVisualizacion visualizador (reloj,reset,reloj_v);
FlipFlopJK ff (1'b1,reloj_v,1'b0,reloj1);
FlipFlopJK ff1 (1'b1,reloj1,1'b0,reloj2);

always @(reloj) 
case ({reloj_v,reloj1,reloj2}) 
{1'b0,1'b0,1'b0}: display1 = 8'b1111_1110; 
{1'b0,1'b0,1'b1}: display1 = 8'b1111_1101; 
{1'b0,1'b1,1'b0}: display1 = 8'b1111_1011; 
{1'b0,1'b1,1'b1}: display1 = 8'b1111_0111;  
{1'b1,1'b0,1'b0}: display1 = 8'b1110_1111;
{1'b1,1'b0,1'b1}: display1 = 8'b1101_1111;
{1'b1,1'b1,1'b0}: display1 = 8'b1011_1111;
{1'b1,1'b1,1'b1}: display1 = 8'b0111_1111;
default: display1=8'b1111_1111;
endcase
endmodule
