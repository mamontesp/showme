`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:26:33 04/10/2016 
// Design Name: 
// Module Name:    relojVisualizacion 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module relojVisualizacion(relojentrada,reset,relojsalida);
input relojentrada;
input reset;
output relojsalida;

reg [10:0] relojaux;
//10

assign relojsalida=relojaux[10];

always @(negedge relojentrada or posedge reset)
if(reset)
begin
relojaux <=10'b000000000;
end
else 
begin
relojaux<=relojaux+1;
end
endmodule
