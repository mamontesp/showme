`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:01:07 11/19/2015 
// Design Name: 
// Module Name:    Register 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module logica(
input [31:0]contador,
input rst,
input temp_correcta,
input [31:0]valorTiempo,
output reg [3:0]numeroInstruccion, 
output [31:0] valor,
input clk,
input [7:0] data,
output [1:0] Mtempp,	//variable que le indicará que temperatura seleccionó el usuario
output reg [3:0] profile, //variable para saber que perfil es (para el uart)
output reg led_listo, //led que notifica al usuario que esta el agua "una RiKuRa" "cosa buena"
output reg Sensor, //le dice al sensor que debe comenzar a medir.
output reg start,//le dice al cronometro que puede iniciar
output reg led_amarillo, led_verde, led_rojo, led_azul, led_ducha,
output reg resetCrono,
output reg parcial,
output reg ducha,
output reg enducha,
output reg calentamiento
	);
reg Mtemp;
assign Mtempp=Mtemp;
reg [31:0]valor1;
reg clkb;
reg [32:0]counter;
reg [4:0] mostrar;
reg [5:0]intervalo;
parameter Counter=50000000;
parameter HELLO=32'h15151825;
parameter INPUT=32'h191E1D25;
parameter PROFILE=32'h0F12150E;
parameter USERA=32'h2121210A;
parameter USERB=32'h2121210B;
parameter USERC=32'h2121210C;
parameter USERD=32'h2121210D;
parameter SELECT=32'h0E0C1D25;
parameter TIPED=32'h0D252525;
parameter TEMP=32'h25252519;
parameter BYE=32'h250B220E;
assign valor=valor1;
initial begin
	counter=0;
	clkb=0;
	Sensor=0;
	start=0;
	led_listo=0;
	parcial=0;
	resetCrono=0;
	enducha=0;
end

always @(posedge clk)
begin
	if(counter == Counter)
			begin
			clkb <= ~clkb;
			counter = 0;
			end
		else
		counter = counter + 1;
end

always @(posedge clk)
begin
	case(mostrar)
				5'b00000: valor1=HELLO;
				5'b00001: valor1=INPUT;
				5'b00010: valor1=PROFILE;
				5'b00011: valor1=USERA;
				5'b00100: valor1=USERB;
				5'b00101: valor1=USERC;
				5'b00110: valor1=USERD;
				5'b00111: valor1=SELECT;
				5'b01000: valor1=TIPED;
				5'b01001: valor1=TEMP;//muestra el los grados centrigrados del Agua
				5'b01011: valor1=contador;//muestra el valor númerico de pulsos del caudal
				5'b01100: valor1=valorTiempo;//muestra el cronometro
				5'b01101: valor1=BYE;
				5'b01111: valor1={24'b0,data};// muestra el valor de la tecla presionada del teclado
				default:  valor1 =32'h25252525;
	endcase

end //end del always
always @(posedge clkb)
begin
	if (!rst)
	intervalo=5'h0;

case(intervalo)
			5'h0:begin 
						numeroInstruccion=4'b1010;//MUESTRA "--HE"
						mostrar=5'b00000;//MUESTRA "LLO-"
						enducha = 0;
						if (data==8'h11)intervalo=5'h1;
						else intervalo=5'h0;
				  end
			5'h1:begin
						numeroInstruccion=4'b1011;//MUESTRA "--IN"
						mostrar=5'b00001;//MUESTRA "PUT-"
						intervalo=5'h2;

				  end
			5'h2:begin
						numeroInstruccion=4'b1100;//MUESTRA "-PRO"
						mostrar=5'b00010;//MUESTRA "FILE-"
				
							if (data==8'h0A)
								begin
								intervalo=5'h3;		
								end
							else if (data==8'h0B)
								begin
								intervalo=5'h4;	
								end
							else if (data==8'h0C)
								begin
								intervalo=5'h5;		
								end
							else if (data==8'h0D)
								begin
								intervalo=5'h6;
								end
							else intervalo=5'h1;
				  end			
			5'h3:begin
						numeroInstruccion=4'b0001;//MUESTRA "USER"
						mostrar=5'b00011;//MUESTRA "___A"
						if(data==8'h25)intervalo=5'h1;
						if (data==8'h11)
						begin
						intervalo=5'h7;
						profile=4'hA;					
						end

				  end
			5'h4:begin
						numeroInstruccion=4'b0001;//MUESTRA "USER"
						mostrar=5'b00100;//MUESTRA "___B"
						if(data==8'h25)intervalo=5'h1;
						if (data==8'h11)
						begin
						intervalo=5'h7;
						profile=4'hB;					
						end
				  end
			5'h5:begin
						numeroInstruccion=4'b0001;//MUESTRA "USER"
						mostrar=5'b00101;//MUESTRA "___C"
						if(data==8'h25)intervalo=5'h1;
						if (data==8'h11)
						begin
						intervalo=5'h7;
						profile=4'hC;					
						end
				  end
			5'h6:begin
						numeroInstruccion=4'b0001;//MUESTRA "USER"
						mostrar=5'b00110;//MUESTRA "___D"
						if(data==8'h25)intervalo=5'h1;
						if (data==8'h11)
						begin
						intervalo=5'h7;
						profile=4'hD;					
						end
				  end
			5'h7:begin
						numeroInstruccion=4'b1101;//MUESTRA "-SEL"
						mostrar=5'b00111;//MUESTRA "ECT-"
						calentamiento=0;
						Mtemp<=2'b00;
						if (data==8'h1) intervalo=5'hA;
						else if (data==8'h2) intervalo=5'h9;
						else if (data==8'h3) intervalo=5'h8;
						else intervalo=5'h7;
				  end
			5'h8:begin
						numeroInstruccion=4'b0110;//MUESTRA "HOT" 
						mostrar =5'b10000;//MUESTRA "----"
						if (data==8'h11)
							begin 
							intervalo=5'hB;
							Mtemp<=2'b11;
							calentamiento=1;
							end	
						else if (data==8'h25)intervalo=5'h7;
						else intervalo=5'h8;
				
				  end
			5'h9:begin
						numeroInstruccion=4'b0111;//MUESTRA "TEPI"
						mostrar=5'b01000;//MUESTRA "D---"
						if (data==8'h11) 
							begin 
							intervalo=5'hB;
							Mtemp<=2'b10;
							calentamiento=1;
							end
						else if (data==8'h25)intervalo=5'h7;
						else intervalo=5'h9;

				  end
			5'hA:begin
						numeroInstruccion=4'b1000;//MUESTRA "COLD"
						mostrar =5'b10000;//MUESTRA "----"
						if (data==8'h11) 
							begin 
							intervalo=5'hB;
							Mtemp<=2'b01;
							calentamiento=1;
							end
						else if (data==8'h25)intervalo=5'h7;
						else intervalo=5'hA;
				
				  end
			5'hB:begin					
						mostrar=5'h9;//MUESTRA  se supone la temperatura "--P"
						calentamiento=1;
						Sensor=1;//activa el sensor para que comience a medir
						if(temp_correcta==1)
						begin
						led_ducha=1;//le indica al usuario que la ducha esta lista
						if (data==8'h11) intervalo=5'hC;
						end
						
				  end
			5'hC:begin
							calentamiento=1;
							ducha=1;					
							led_ducha=0;//desactiva el led de notificación
							start=1;//comienza el crono
							numeroInstruccion=4'b1001; //muestra Time
							mostrar=5'b01100;//el cronometro
							if 	(valorTiempo[15:8]== 8'd3)mostrar=5'b01001;
							if	(valorTiempo[15:8]==8'd0)mostrar=5'b01100;
							if	(valorTiempo[23:16]== 8'd2)begin led_azul=1;end
							if	(valorTiempo[23:16]== 8'd4)begin led_verde=1;led_azul=0;end
							if	(valorTiempo[23:16]== 8'd6)begin led_amarillo=1; led_verde=0;end 
							if	(valorTiempo[23:16]== 8'd8)begin led_rojo=1; led_amarillo=0; end
							if 	(valorTiempo[23:16]== 8'd9)begin led_rojo=~led_rojo;end
							if	(valorTiempo[31:24]== 8'd1 && valorTiempo[23:16]== 8'd0)
								begin 
								ducha=0;
								calentamiento=0;
								parcial=1;
								resetCrono=1;
								led_rojo=0;
								numeroInstruccion=4'b1111;//muestra "Good" 
								mostrar=5'b01101;// muestra "-bye"
								intervalo=5'hD;
								enducha = 1;
								Mtemp<=0;		
								end			
					
							if (data==8'h11 && ducha==1) intervalo=5'hC;
							if (data==8'h0)
									begin 
										ducha=0;
										calentamiento=0;
										parcial=1;
										resetCrono=1;
										led_rojo=0;
										led_amarillo=0;
										led_verde=0;
										led_azul=0;
										numeroInstruccion=4'b1111;//muestra "Good" 
										mostrar=5'b01101;// muestra "-bye"
										intervalo=5'hD;
										enducha = 1;
							
									end		
				end
			5'hD:begin
				if (data==8'h11) intervalo=5'h0;
				else if (data==8'h5) 
				begin
				numeroInstruccion=4'b0000;//muestra "_____" 
				mostrar=5'b01011;// muestra "contador"					
				end
				else intervalo=5'hD;
				
				end
			default: 
				begin
				numeroInstruccion=4'b1111;//muestra "Good" 
				mostrar=5'b01101;// muestra "-bye"	
				end
		endcase
end


endmodule
