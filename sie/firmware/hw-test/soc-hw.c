#include "soc-hw.h"
#include "math.h"
///Casteo de las direcciones para cada periférico

uart_t   *uart0  = (uart_t *)   0x60000000;
timer_t  *timer0 = (timer_t *)  0x40000000;
gpio_t   *gpio0  = (gpio_t *)   0x20000000;
onew_t   *onew0  = (onew_t *)   0x80000000;

isr_ptr_t isr_table[32];

void tic_isr();
/***************************************************************************
 * IRQ handling
 */

//Interrupciones

void isr_null()
{
}

void irq_handler(uint32_t pending)
{
	int i;

	for(i=0; i<32; i++) {
		if (pending & 0x01) (*isr_table[i])();
		pending >>= 1;
	}
}

void isr_init()
{
	int i;
	for(i=0; i<32; i++)
		isr_table[i] = &isr_null;
}

void isr_register(int irq, isr_ptr_t isr)
{
	isr_table[irq] = isr;
}

void isr_unregister(int irq)
{
	isr_table[irq] = &isr_null;
}

/***************************************************************************
 * TIMER Functions
 */
void msleep(uint32_t msec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000)*msec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}

void nsleep(uint32_t nsec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000000)*nsec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}


uint32_t tic_msec;

void tic_isr()
{
	tic_msec++;
	timer0->tcr0     = TIMER_EN | TIMER_AR | TIMER_IRQEN;
}

void tic_init()
{
	tic_msec = 0;

	// Setup timer0.0
	timer0->compare0 = (FCPU/10000);
	timer0->counter0 = 0;
	timer0->tcr0     = TIMER_EN | TIMER_AR | TIMER_IRQEN;

	isr_register(1, &tic_isr);
}
//pasar todo a segundos
void tiempo_ducha()
{
	int tiempo;
	int P1s1;
	int P1s2;
	int P1min1;
	int P1min2;
	int enducha;
	enducha = timer0->enducha;
	if (enducha == 1){
	P1s1 = timer0->P1s1;
	P1s2 = timer0->P1s2;
	P1min1 = timer0->P1min1;
	P1min2 = timer0->P1min2;
	tiempo = P1s1+(P1s2*10)+(P1min1*60)+(P1min2*600);
	}
	else
	{
	tiempo = 0;
	}
consumo(tiempo);
}

/***************************************************************************
* Gpio Functions
*/

char read_data()
        {
	return gpio0->gpio_i;
}
char read_profile()
        {

	if(timer0->enducha==1){
	return gpio0->profile;
	}
}


int read_temp()
{
	return gpio0->tempSel;
}
void prueba(char c)
{
	gpio0->gpio_dir=c;
	gpio0->gpio_o=c;
}

void write_data(char c)
        {
	gpio0->gpio_o=c;
}

void write_fr(char c1, char c2, char c3)
        {
	gpio0->fr1=c1;
	gpio0->fr2=c2;
	//gpio0->fr3=c3;
}

void write_dir(char c)
        {
	gpio0->gpio_dir=c;
}
void send_key(char c)
	{
	gpio0->files=c;
	} 

int modulo(int a, int b)
{
	int aux;
	while(a>=b)
	{
		aux=a-b;
		a=aux;
	}
	return a;
}

int div(int a, int b){
		int i=0;
		while (a>=b){
			i++;
			a=a-b;
		}
		return i;
	}
	//Máximo Común Divisor
	int MCD(int a, int b)
	{
	int iaux; //auxiliar
	int i1=a>b?a:b;
	//max(a,b);
	int i2=a<=b?a:b;
	//min(a,b);
	do{
		iaux=i2;
		//i2=i1-i2*(i1/i2)
		i2=modulo(i1,i2);
		i1=iaux;
		}while(i2!=0);
	return i1;
}

//Calculo de la Temperatura
void calc_temp(int Ts)
{
		int Tcal=50;
		int Tfr=10;
		int fr1;
		int fr2;
		//int fr3;
		//int32_t Ts;
		//output
		//int32_t fr1;
		//int32_t fr2;
		//CambioTemperaturaPerdida
		int templost;
		//CambioTemperaturaGanada
		int tempgain;
		templost=Tcal-Ts;
		tempgain=Ts-Tfr;
		write_data(templost);
		write_data(tempgain);
		uint32_t mcd;
		mcd=MCD(templost,tempgain);
		fr1=div(templost,mcd);
		fr2=div(tempgain,mcd);
		write_fr(fr1,fr2, 0X01);
		//gpio0->fr3=1;
}

int consumo (int a) //funcion para el consumo
{
	int pulsos;
	int freq;
	int consumo;
	pulsos = gpio0->contador;
	freq=div(pulsos,a);
	consumo = div(freq*1700,7);
	return consumo;	
}


/***************************************************************************
 * UART Functions
 */
void uart_init()
{
//uart0->ier = 0x00;  // Interrupt Enable Register
//uart0->lcr = 0x03;  // Line Control Register:    8N1
//uart0->mcr = 0x00;  // Modem Control Register

// Setup Divisor register (Fclk / Baud)
//uart0->div = (FCPU/(57600*16));
}

char uart_getchar()
{   
	while (! (uart0->ucr & UART_DR)) ;
	return uart0->rxtx;
}

void uart_putchar(char c)
{
	while (uart0->ucr & UART_BUSY) ;
	uart0->rxtx = c;
}

void uart_putstr(char *str)
{
	char *c = str;
	while(*c) {
		uart_putchar(*c);
		c++;
	}
}

/************Funciones de One Wire*******************/
/*Máximo Común Múltiplo*/



