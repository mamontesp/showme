#ifndef SPIKEHW_H
#define SPIKEHW_H

#define PROMSTART 0x00000000
#define RAMSTART  0x00000800
#define RAMSIZE   0x400
#define RAMEND    (RAMSTART + RAMSIZE)

#define RAM_START 0x40000000
#define RAM_SIZE  0x04000000

#define FCPU      50000000

#define UART_RXBUFSIZE 32

/****************************************************************************
 * Types
 */
typedef unsigned int  uint32_t;    // 32 Bit
typedef signed   int   int32_t;    // 32 Bit

typedef unsigned char  uint8_t;    // 8 Bit
typedef signed   char   int8_t;    // 8 Bit

/****************************************************************************
 * Interrupt handling
 */
typedef void(*isr_ptr_t)(void);

void     irq_enable();
void     irq_disable();
void     irq_set_mask(uint32_t mask);
uint32_t irq_get_mak();

void     isr_init();
void     isr_register(int irq, isr_ptr_t isr);
void     isr_unregister(int irq);

/****************************************************************************
 * General Stuff
 */
void     halt();
void     jump(uint32_t addr);


/****************************************************************************
 * Timer
 */
#define TIMER_EN     0x08    // Enable Timer
#define TIMER_AR     0x04    // Auto-Reload
#define TIMER_IRQEN  0x02    // IRQ Enable
#define TIMER_TRIG   0x01    // Triggered (reset when writing to TCR)

typedef struct {
	volatile uint32_t tcr0;
	volatile uint32_t compare0;
	volatile uint32_t counter0;
	volatile uint32_t tcr1;
	volatile uint32_t compare1;
	volatile uint32_t counter1;
	volatile uint32_t enducha;
	volatile uint32_t P1s1; //cambios aca
	volatile uint32_t P1s2;
	volatile uint32_t P1min1;
	volatile uint32_t P1min2;
} timer_t;

void msleep(uint32_t msec);
void nsleep(uint32_t nsec);

void tic_init();
void tiempo_ducha(); //cambio


/*****************************GPIO0***********************/
typedef struct {
	volatile uint32_t gpio_i;
	volatile uint32_t gpio_o;
	volatile uint32_t gpio_dir;
	volatile uint32_t fr1;
	volatile uint32_t fr2;
	volatile uint32_t fr3;
	volatile uint32_t files;
	volatile uint32_t data;
	volatile uint32_t contador;
	volatile uint32_t tempSel;
	volatile uint32_t profile;
	} gpio_t;

char read_data();
int read_temp();
char read_profile();
void write_data(char c);
void write_dir(char c);
void send_key(char c);
void prueba(char c);
void calc_temp(int Ts);
int consumo (int a); //cambio
/***************************UART0****************/
#define UART_DR   0x01                    // RX Data Ready
#define UART_ERR  0x02                    // RX Error
#define UART_BUSY 0x10                    // TX Busy

typedef struct {
   volatile uint32_t ucr;
   volatile uint32_t rxtx;

} uart_t;

void uart_init();
void uart_putchar(char c);
void uart_putstr(char *str);
char uart_getchar();


/****************************OneWire*************/
typedef struct {
	volatile uint32_t onew_i;
	volatile uint32_t onew_dir;
	volatile uint32_t temp;
	
} onew_t;

//int32_t MCD(int32_t a, int32_t b);
//void calc_temp(uint32_t c);
/***************************************************************************
 * Pointer to actual components
 */
extern timer_t  *timer0;
extern uart_t   *uart0; 
extern gpio_t   *gpio0; 
extern onew_t	*onew0;
extern uint32_t *sram0; 

#endif // SPIKEHW_H
